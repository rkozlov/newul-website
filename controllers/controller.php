<?php
	header("X-Powered-By:");
	header("Content-Security-Policy:");
	header("Referrer-Policy:");
	header("Strict-Transport-Security:");
	header("X-Content-Type-Options:");
	header("X-Frame-Options:");
	header("X-XSS-Protection: 1; mode=block");
	error_reporting(0);
	$request_url = explode('?', $_SERVER['REQUEST_URI']);
	$url = $request_url[0];